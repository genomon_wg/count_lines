#CC = gcc
#CFLAGS = -g -Wall -fopenmp
CC = fccpx
CFLAGS = -Nclang -Ofast -fopenmp

#CFLAGS += -DDIRECT_IO

PROGRAM = count_lines

OBJS = count_lines.o

all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

.c.o:
	$(CC) $(CFLAGS) -c $<

.PHONY: test
test: $(PROGRAM)
	cd test && ./test.sh

clean:
	rm -f $(OBJS)

distclean: clean
	rm -f $(PROGRAM)
	rm -rf *.dSYM
	rm -f test/run_*.sh.*
