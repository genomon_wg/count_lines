#!/bin/bash -x
#PJM --rsc-list "rscgrp=small"
#PJM --rsc-list "node=12"
#PJM --rsc-list "node-quota=29G"
#PJM --rsc-list "elapse=2:00:00"
#PJM --stg-transfiles all
#PJM --stgin "../count_lines ./"
#PJM -m b,e
#PJM -s

. /work/system/Env_base

export XOS_MMM_L_ARENA_LOCK_TYPE=0
export XOS_MMM_L_ARENA_FREE=1
export OMP_NUM_THREADS=8

SCRATCH=/scratch/ra000022/a03243
ATL_DIR=${SCRATCH}/GPK/ATL
input0=${ATL_DIR}/Miya1T/1.fastq  # 181GB
#input0=${SCRATCH}/TEST/sequence1.fastq # 18G

export TIMEFORMAT=$'time(real,user,sys) = %R\t%U\t%S'

time cp $input0 in.fastq

lfs getstripe in.fastq 1>&2

bufsize_list="1 2 4 8"
for bufsize in $bufsize_list
do
        echo $bufsize 1>&2
        time ./count_lines in.fastq $bufsize 1>&2
done
