#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define BUFSIZE (256 * 1024 * 1024L)

off_t get_file_size(const char *file)
{
	int fd = open(file, O_RDONLY, 0);
	if (fd < 0) {
		perror(file);
		exit(1);
	}
	struct stat buf;
	fstat(fd, &buf);
	close(fd);
	return buf.st_size;
}


size_t count_lines_mem(size_t n, const char* buf)
{
	size_t lines = 0;
	const char *p, *q;
	p = buf;
	while (n > 0 && (q = memchr(p, '\n', n)) != NULL) {
		lines++;
		q++;
		n -= q - p;
		p = q;
	}
	return lines;
}


size_t count_lines(const char *file, off_t bufsize)
{
	size_t lines = 0;
	off_t file_size = get_file_size(file);

#ifdef DIRECT_IO
	int fd = open(file, O_RDONLY | O_DIRECT, 0);
#else
	int fd = open(file, O_RDONLY, 0);
#endif
	if (fd < 0) {
		perror(file);
		exit(1);
	}

#pragma omp parallel reduction(+:lines)
	{
	char *buf;
	int ret = posix_memalign((void *)&buf, 1024*64, bufsize);
	if (ret < 0) {
		perror("buf");
		exit(1);
	}

#pragma omp for schedule(dynamic,1)
	for (off_t offset = 0; offset < file_size; offset += bufsize) {
		ssize_t n = pread(fd, buf, bufsize, offset);
		assert(n >= 0);
		lines += count_lines_mem(n, buf);
	}
	free(buf);

	} /* omp parallel */

	close(fd);

	return lines;
}


int main(int argc, char *argv[])
{
	if (!(argc == 2 || argc == 3)) {
		fprintf(stderr, "usage: %s file [bufsize(in MiB)]\n", argv[0]);
		exit(1);
	}

	const char *file = argv[1];

	off_t bufsize = BUFSIZE;
	if (argc == 3) bufsize = 1024L * 1024 * atoi(argv[2]);
	//printf("bufsize: %d MiB\n", atoi(argv[2]));

	size_t lines = count_lines(file, bufsize);
	//printf("# of lines: %ld\n", lines);
	
	printf("%ld\n", lines);

	return 0;
}
