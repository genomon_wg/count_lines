# count_lines

高速ファイル行数カウントプログラム。

## Usage

    count_lines <file> [<bufsize>]

bufsizeの単位はMiB。
省略時のディフォルト値は256MiB。
